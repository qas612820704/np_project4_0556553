#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iostream>
#include <netdb.h>
#include <sstream>

#include <arpa/inet.h>
#include <csignal>
#include <netdb.h>
#include <netinet/in.h>
#include <string>
#include <sys/socket.h>
#include <sys/wait.h>
#include <unistd.h>

#include "include/define.h"
#include "include/ultis.h"

using namespace std;

int proxy_server(int sockfd) {
  int pid = getpid();

  unsigned char buf[MAX_LINE];
  char buf_b[MAX_LINE], buf_p[MAX_LINE];

  int len;

  bzero(buf, MAX_LINE);
  len = read(sockfd, buf, MAX_LINE);
  if (len == 0) {
    err_sys("sock v4 request error");
  }

  unsigned char VN = buf[0];
  unsigned char CD = buf[1];
  unsigned int PROXY_PORT = buf[2] << 8 | buf[3];
  unsigned int PROXY_IP = buf[7] << 24 | buf[6] << 16 | buf[5] << 8 | buf[4];
  char *USER_ID = (char *)buf + 8;

  if (VN != 0x04) {
    err_sys("Not Sock v4!");
  }

  int proxyfd;
  sockaddr_in proxy_addr;
  unsigned int proxy_addr_len = sizeof(proxy_addr);
  bzero(&proxy_addr, proxy_addr_len);
  unsigned char sockv4_reply_pkg[8];

  bool is_conn_mode = false;
  bool is_bind_mode = false;
  bool is_pass[4];
  bool is_firewall_pass;

  if (CD == 0x01) {
    is_conn_mode = true;
    is_firewall_pass = true;

    proxy_addr.sin_family = AF_INET;
    proxy_addr.sin_port = htons(PROXY_PORT);
    proxy_addr.sin_addr.s_addr = PROXY_IP;

    if ((proxyfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
      err_sys("server: can't open stream socket");

    if (connect(proxyfd, (sockaddr *)&proxy_addr, sizeof(proxy_addr)) < 0)
      err_sys("server: can't connect");

    int addr[4];
    addr[0] = proxy_addr.sin_addr.s_addr % 256;
    addr[1] = (proxy_addr.sin_addr.s_addr >> 8) % 256;
    addr[2] = (proxy_addr.sin_addr.s_addr >> 16) % 256;
    addr[3] = proxy_addr.sin_addr.s_addr >> 24;

    ifstream ifs("socks.conf");
    while (ifs) {
      string str_ip;
      getline(ifs, str_ip);
      istringstream ip_tok(str_ip.c_str());

      int conf_addr[4];
      for (int i = 0; i < 4; i++) {
        string partial_ip;
        getline(ip_tok, partial_ip, '.');
        if (partial_ip == "*") {
          conf_addr[i] = -1;
        } else {
          conf_addr[i] = atoi(partial_ip.c_str());
        }

        if ((conf_addr[i] < 0) || (conf_addr[i] == addr[i])) is_pass[i] = true;

        is_firewall_pass = is_firewall_pass && is_pass[i];
      }
      if (is_firewall_pass) break;
    }
    sockv4_reply_pkg[0] = 0;
    if (is_firewall_pass)
      sockv4_reply_pkg[1] = 0x5A;
    else
      sockv4_reply_pkg[1] = 0x5B;
    sockv4_reply_pkg[2] = buf[2];
    sockv4_reply_pkg[3] = buf[3];
    sockv4_reply_pkg[4] = buf[4];
    sockv4_reply_pkg[5] = buf[5];
    sockv4_reply_pkg[6] = buf[6];
    sockv4_reply_pkg[7] = buf[7];
    write(sockfd, sockv4_reply_pkg, 8);

  } else if (CD == 0x02) {
    is_bind_mode = true;
    is_firewall_pass = false;

    int myfd;
    sockaddr_in my_addr;
    unsigned int my_addr_len;
    bzero(&my_addr, sizeof(my_addr));

    if ((myfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
      err_sys("server: can't open stream socket");

    my_addr.sin_family = AF_INET;
    my_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    my_addr.sin_port = htonl(INADDR_ANY);

    if (bind(myfd, (sockaddr *)&my_addr, sizeof(my_addr)) < 0)
      err_sys("Unable bind");

    if (getsockname(myfd, (sockaddr *)&proxy_addr, &proxy_addr_len) < 0)
      err_sys("Unable getsockname");

    if (listen(myfd, MAX_CLIENT) < 0) err_sys("Unable listen");

    sockv4_reply_pkg[0] = 0;
    sockv4_reply_pkg[1] = 0x5A;
    sockv4_reply_pkg[2] = (unsigned char)(ntohs(proxy_addr.sin_port) / 256);
    sockv4_reply_pkg[3] = (unsigned char)(ntohs(proxy_addr.sin_port) % 256);
    sockv4_reply_pkg[4] = 0;
    sockv4_reply_pkg[5] = 0;
    sockv4_reply_pkg[6] = 0;
    sockv4_reply_pkg[7] = 0;
    write(sockfd, sockv4_reply_pkg, 8);

    if ((proxyfd = accept(myfd, (sockaddr *)&proxy_addr, &proxy_addr_len)) < 0)
      err_sys("Unable Accept");

    write(sockfd, sockv4_reply_pkg, 8);

  } else {
    err_sys("Unacceptable CD!");
  }
  cout << "<D_IP>    : " << inet_ntoa(proxy_addr.sin_addr) << endl;
  cout << "<D_PORT>  : " << ntohs(proxy_addr.sin_port) << endl;
  if (is_conn_mode) cout << "<Command> : CONNECT" << endl;
  if (is_bind_mode) cout << "<Command> : BIND" << endl;
  if (is_firewall_pass)
    cout << "<Reply>   : Accept" << endl;
  else
    cout << "<Reply>   : Reject" << endl;
  cout << endl;

  fd_set rfds, afds;

  int nfds = (sockfd > proxyfd ? sockfd : proxyfd) + 1;

  FD_ZERO(&rfds);
  FD_ZERO(&afds);
  FD_SET(sockfd, &afds);
  FD_SET(proxyfd, &afds);

  bool is_conn = true;

  while (is_conn) {
    len = 0;
    memcpy(&rfds, &afds, sizeof(rfds));
    bzero(&buf_b, MAX_LINE);
    bzero(&buf_p, MAX_LINE);

    if (select(nfds, &rfds, (fd_set *)0, (fd_set *)0, (timeval *)0) < 0)
      err_sys("select: error");

    if (FD_ISSET(sockfd, &rfds)) {
      len = read(sockfd, buf_b, MAX_LINE);
      if (len == 0) {
        is_conn = false;
        close(sockfd);
        FD_CLR(sockfd, &afds);
        close(proxyfd);
        FD_CLR(proxyfd, &afds);
      } else if (len < 0) {
        close(sockfd);
        FD_CLR(sockfd, &afds);
      } else {
        write(proxyfd, buf_b, len);
      }
    }
    if (FD_ISSET(proxyfd, &rfds)) {
      len = read(proxyfd, buf_p, MAX_LINE);
      if (len == 0) {
        is_conn = true;
        close(sockfd);
        FD_CLR(sockfd, &afds);
        close(proxyfd);
        FD_CLR(proxyfd, &afds);
      } else if (len < 0) {
        close(proxyfd);
        FD_CLR(proxyfd, &afds);
      } else {
        write(sockfd, buf_p, len);
      }
    }
  }
  return 0;
}
