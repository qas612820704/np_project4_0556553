SDIR= src
IDIR= $(SDIR)/include

CC= g++
CFLAGS=-I$(IDIR)

DBG=-g -D DEBUG

ODIR=$(SDIR)/obj
LDIR=$(SDIR)/lib
BDIR=bin

SRC= $(shell find $(SDIR) -type f -name '*.cpp')
OBJ= $(patsubst $(SDIR)/%.cpp,$(ODIR)/%.o,$(SRC))
DBGOBJ= $(patsubst $(SDIR)/%.cpp,$(ODIR)/%-dbg.o,$(SRC))
all: $(BDIR)/server

.PHONY: debug
debug: $(BDIR)/server-dbg
	gdb $(BDIR)/server-dbg

$(BDIR)/server: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS)

$(ODIR)/%.o: $(SDIR)/%.cpp
	@mkdir -p $(ODIR)
	$(CC) -c -o $@ $< $(CFLAGS)

$(BDIR)/server-dbg: $(DBGOBJ)
	$(CC) $(DBG) -o $@ $^ $(CFLAGS)

$(ODIR)/%-dbg.o: $(SDIR)/%.cpp
	$(CC) $(DBG) -c -o $@ $< $(CFLAGS)

.PHONY: start clean
start:
	$(BDIR)/server

clean:
	rm $(ODIR)/*
	rm $(BDIR)/*
