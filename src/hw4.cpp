#include <cstdlib>
#include <cstring>
#include <iostream>
#include <netdb.h>

#include <arpa/inet.h>
#include <csignal>
#include <netdb.h>
#include <netinet/in.h>
#include <string>
#include <sys/socket.h>
#include <sys/wait.h>
#include <unistd.h>

#include "include/define.h"
#include "include/proxy_server.h"
#include "include/ultis.h"

using namespace std;

int create_server(int port) {
  int listen_fd;

  sockaddr_in server_addr;

  bzero(&server_addr, sizeof(server_addr));

  server_addr.sin_family = AF_INET;
  server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
  server_addr.sin_port = htons(port);

  if ((listen_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    err_sys("server: can't open stream socket");

  if (bind(listen_fd, (sockaddr *)&server_addr, sizeof(server_addr)) < 0) {
    err_sys("server: can't bind address", to_string(port).c_str(), NULL);
  }

  listen(listen_fd, MAX_CLIENT);

  return listen_fd;
}

int main(int argc, char const *argv[]) {
  int port = PORT;
  if (argc >= 2) {
    port = atoi(argv[1]);
  }

  sockaddr_in cli_addr;
  unsigned int cli_addr_len = sizeof(cli_addr);

  int masterfd, connfd;
  int pid;

  masterfd = create_server(port);

  while (true) {

    bzero(&cli_addr, cli_addr_len);

    if ((connfd = accept(masterfd, (sockaddr *)&cli_addr, &cli_addr_len)) < 0)
      err_sys("server: accpet error");

    if ((pid = fork()) < 0)
      err_sys("server: fork error");
    else if (pid == 0) {
      close(masterfd);

      cout << "<S_IP>    : " << inet_ntoa(cli_addr.sin_addr) << endl;
      cout << "<S_PORT>  : " << ntohs(cli_addr.sin_port) << endl;
      proxy_server(connfd);

      close(connfd);

      exit(0);
    } else {
      close(connfd);
    }
  }

  return 0;
}
